# ChangeLog

## Unreleased

## 8.0.3 (2024-06-06)

**Fixed:**
* Dependency chaos with jackson library

## 8.0.2 (2024-05-31)

**Changed:**
* Use proper API request and response validation

## 8.0.1 (2024-05-29)

**Changed:**
* External run status (API) only contains pipe header 

## 8.0.0 (2024-05-27)

**Changed:**
* Run definition, run id definition, and run API (breaking changes)

**Added:**
* End time for runs 
* Tests for run API

**Fixed:**
* Default forward protocol if not specified

## 7.1.2 (2024-04-16)

**Added:**
* Exception handling in forward verticle

**Changed:**
* README examples in "Getting Started"

## 7.1.1 (2024-02-14)

**Added:**
* Configuration for http body size limit

**Fixed:**
* Vert.x update ([CVE-2024-1300](https://access.redhat.com/security/cve/CVE-2024-1300))

## 7.1.0 (2023-12-20)

**Added:**
* Support for OpenTelemetry

## 7.0.8 (2023-11-10)

**Changed:**
* Update model library with increased json string size limit

## 7.0.7 (2023-11-06)

**Changed:**
* Try to keep run inside cache alive during processing

## 7.0.6 (2023-06-30)

**Changed:**
* Pass buffer directly in the forward verticle without additional stringification

## 7.0.5 (2023-05-04)

**Changed:**
* Update pipe model for increased max string size limit  

## 7.0.4 (2023-02-12)

**Changed:**
* Bump up lib dependencies

## 7.0.3 (2022-12-21)

**Changed:**
* Read build info once on startup for faster health check

## 7.0.2 (2022-11-20)

**Added:**
* A default `buildInfo.json` to properly serve the health check

## 7.0.1 (2022-09-27)

**Fixed:**
* Run status failed with correct status string

**Changed:**
* Forward decision for simple handler

## 7.0.0 (2022-08-31)

**Changed:**
* Refactored pipe handler
* Asynchronous response 202 with string, Synchronous response 200 with json object

**Added:**
* Overloaded method for `publishTo`
* Run cancellation (run cache, run status)
* API for run cancellation and status

## 6.1.0 (2022-06-28)

**Changed:**
* Handling bad request output

**Added:**
* Ability to install synced handler, returning json

## 6.0.1 (2022-04-19)

**Changed:**
* Lib updates

## 6.0.0 (2022-02-14)

**Removed:**
* Metrics dropwizard dependency
* Metrics endpoint

## 5.6.0 (2022-01-09)

**Removed:**
* Forward and pass via own web client

## 5.5.1 (2021-10-09)

**Changed:**
* Model manager update

## 5.5.0 (2021-10-09)

**Changed:**
* Non-blocking body parsing only when size is exceeded a specific limit

## 5.4.0 (2021-06-23)

**Changed:**
* PipeContext eventbus handling
* Non-blocking body parsing

## 5.3.2 (2021-06-10)

**Changed:**
* Update pipe model lib

## 5.3.1 (2021-06-02)

**Changed:**
* Lib dependencies update

## 5.3.0 (2021-05-05)

**Added:**
* Additional smtp configuration possibility

**Changed:**
* Title of OpenAPI description

## 5.2.1 (2021-03-03)

**Fixed:**
* Support for a configurable logo and favicon

## 5.2.0 (2021-02-19)

**Added:**
* Support for a configurable logo and favicon

## 5.1.0 (2021-02-07)

**Fixed:**
* Sub-router connection

## 5.0.0 (2021-01-07)

**Changed:**
* Migrate to Vert.x 4.0.0

## 4.3.0 (2020-11-12)

**Added:**
* Log level checks in `PipeLogger`

## 4.2.4 (2020-09-03)

**Fixed:**
* Calling handler correctly 

## 4.2.2 (2020-09-01)

**Changed:**
* Update model lib to Kotlin 1.4.0

## 4.2.1 (2020-08-31)

**Changed:**
* Use send on the eventbus again

## 4.2.0 (2020-07-13)

**Added:**
* Overloaded functions
* New pipe model dependency

**Changed:**
* Handler signature
* Deprecate `consumer(address)` method, rename it to `publishTo(address)`

## 4.1.0 (2020-06-11)

**Changed:**
* Use of `PipeManager` API
* Use always model object mapper
* Use publish to propagate incoming pipe
* Change `consumer` to`publishTo`

**Added:**
* Setting start time again

## 4.0.1 (2020-05-27)

**Removed:**
* Setting start time
 
## 4.0.0 (2020-05-27)

**Added:**
* Quality check job in gitlab ci

**Fixed:**:
* Overload and static create methods for connector

**Changed:**
* Pipe log pattern. Removed pipe and segment title, add pipe name

## 3.0.3 (2020-04-02)

**Changed**:
* Vert.x and dependencies update

## 3.0.2 (2020-03-03)

**Fixed:**
* Pass payload implicitly

## 3.0.1 (2020-02-28)

**Fixed:**
* Return non null Unit in pass and forward
* Forward now via a separate verticle

## 3.0.0 (2020-01-24)

**Added:**
* Implicit web client, allowing `forward()` in most cases
* `setResult` methods which accepts vertx `JsonObject` for `dataInfo` parameter

**Removed:**
* `forward(vertx)` and `pass(vertx)` due to dangerous reasons. Deprecation seems not enough.

**Changed:**
* `config` and `dataInfo` are returned now as vertx json objects 

## 2.0.3 (2019-10-16)

**Added:**
* Possibility to mount a sub-router to the connector

## 2.0.2 (2019-10-02)

**Fixed:**
* Failed check negated

## 2.0.1 (2019-08-28)

**Changed**
* Requires now latest LTS Java 11
* Update pipe model to 2.1.0

## 2.0.0 (2019-08-22)

**Changed**
* `PipeLogger`, `PipeContext` and `PipeMailer` classes implemented in kotlin
* Updated to pipe-model version 2.0.0. This forces a major version update as well!

## 1.0.4 (2019-08-13)

**Fixed:**
* Another hotfix for deep copy of pipe.

## 1.0.3 (2019-08-12)

**Fixed:**
* Hotfix for deep copy of pipe.

## 1.0.2 (2019-08-12)

**Fixed:**
* `setPayloadData` passing non null value of `DataType` (kotlin)

## 1.0.1 (2019-08-09)

**Fixed:**
* Update pipe-model, fix for Pipe deserialization

## 1.0.0 (2019-08-09)

**Added:**
* kotlin support
* Test stage as part of gitlab ci

**Changed:**
* Upgrade to Vert.x 3.8.0
* Upgrade to kotlin 1.3.41 (no implementations yet)
* Upgrade to pipe-model 1.0.0 (pure kotlin)
 
## 0.0.4 (2019-??-??)

**Added:**
* `/health` path up status with build info
* `/metrics` path with metrics info
* `/config/schema` returns config json schema

**Changed:**
* Log output in case of forwarding
* Upgrade to Vert.x 3.7.1

**Fixed:**
* Resource filtering before build

## 0.0.3 (2019-04-15)

**Added:**
- EMail support
- Configuration for the release plugin in the pom file
- ChangeLog

**Changed:**
- Upgrade to Vert.x 3.7.0

## 0.0.1 (2019-03-01)

Initial Release
