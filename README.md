# piveau pipe connector

## Features

* An asynchronous event based pipe handling
* piveau conformable logging
* Internal management of pipe structure
* Convenient API
* Vert.x support

## Get started

The simplest example would be (in java):

```java
PipeConnector.create().handlePipe(pipeContext -> pipeContext.setResult("Hello World!").forward());
```

Of course, a more practical example would be this (in Kotlin):

```kotlin
val connector = createPipeConnector();
connector.handlePipe { pipeContext: PipeContext ->
    val config = pipeContext.config
    val data = pipeContext.stringData

    // do your job here
    val result = ...

  pipeContext.setResult(result).forward()
}
```

If you're already have a Vert.x instance and would like to use a worker Verticle with an event bus address, this will
do:

```kotlin
PipeConnector
    .create(vertx)
    .onSuccess { connector -> connector.publishTo(EVENTBUS_ADDRESS) }
```

And on the receiver verticle side:

```kotlin
override suspend fun start() {
    vertx.eventBus().coConsumer(EVENTBUS_ADDRESS) {
        handlePipe(it) 
    }
}

private suspend fun handlePipe(message: Message<PipeContext>) {}
```

## Service Interface

* `/pipe`

  Pipe endpoint (see ReDoc on root context path)

* `/health`

  Returns up status and build infos. Requires a buildInfo.json in the classpath, e.g.:
    ```json
    {
       "timestamp": "${buildTimestamp}",
       "version": "${project.version}"
    }
    ```
  `timestamp` and `version` could be set automatically by maven.

* `/metrics`

  Internal metric infos

It is possible to mount a sub-router for your own (additional) interfaces:

```java
connector.subRouter("/mountPoint", router);
```

## Configuration

 * PIVEAU_PIPE_ENDPOINT_PORT - Default port is 8080
 * PIVEAU_PIPE_MAIL_CONFIG - JSON object:
   
   ```json
   {
      "host": "<smtp_host>",
      "system": "<environment>",
      "mailto": "<receiver_address>"
   }
   ```
 * PIVEAU_FAVICON_REF - Reference to a favicon asset
 * PIVEAU_LOGO_REF - Reference to a logo asset
 * PIVEAU_PIPE_BODY_THRESHOLD - Threshold size for synchronous content parsing. Default is 3145728 bytes.
 * PIVEAU_PIPE_HTTP_BODY_LIMIT - HTTP content size limit before responding with 413. -1 means unlimited. Default is 20MB
 