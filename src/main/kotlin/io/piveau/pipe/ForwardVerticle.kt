package io.piveau.pipe

import io.vertx.core.AbstractVerticle
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.Message
import io.vertx.core.http.HttpHeaders
import io.vertx.core.http.HttpMethod
import io.vertx.core.tracing.TracingPolicy
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClientOptions
import org.slf4j.LoggerFactory
import java.net.MalformedURLException
import java.net.URL

class ForwardVerticle : AbstractVerticle() {
    private val log = LoggerFactory.getLogger(javaClass)

    private lateinit var webClient: WebClient

    override fun start() {
        vertx.eventBus().consumer(ADDRESS, ::handleForward)
        webClient = WebClient.create(vertx, WebClientOptions().setTracingPolicy(TracingPolicy.PROPAGATE))
    }

    private fun handleForward(message: Message<Buffer>) {
        val pipeManager = PipeManager.read(message.body().toString())
        pipeManager.currentEndpoint?.apply {
            val address = try {
                URL(address)
            } catch (e: MalformedURLException) {
                log.error("Reading configured endpoint address", e)
                return
            }

            val method = try {
                HttpMethod.valueOf(method ?: "POST")
            } catch (e: IllegalArgumentException) {
                log.warn("Unknown http method: {}. Using POST instead", method)
                HttpMethod.POST
            }

            if (log.isTraceEnabled) {
                log.trace("Pipe to forward: {}", pipeManager.prettyPrint())
            }

            webClient.request(method, address.port, address.host, address.path)
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), "application/json")
                .sendBuffer(message.body())
                .onSuccess {
                    when (it.statusCode()) {
                        in 200..299 -> log.trace("Successfully forwarded to {}", address)
                        400, 409 -> log.error("Forwarding failed: {} - {}", it.statusMessage(), it.bodyAsJsonObject() ?: "no info")
                        else -> log.error("Forwarding failed: {}", it.statusMessage())
                    }
                }
                .onFailure { log.error("Forwarding failed", it) }
        } ?: log.trace("No next pipe segment available")
    }

    companion object {
        const val ADDRESS: String = "io.piveau.pipe.forward.queue"
    }

}