package io.piveau.pipe.connector

import io.netty.handler.codec.http.HttpResponseStatus
import io.piveau.pipe.*
import io.piveau.pipe.PipeMailer.Companion.create
import io.piveau.pipe.handler.EventbusPipeHandler
import io.piveau.pipe.handler.PipeHandler
import io.piveau.pipe.handler.SimplePipeHandler
import io.vertx.config.ConfigRetriever
import io.vertx.config.ConfigRetrieverOptions
import io.vertx.config.ConfigStoreOptions
import io.vertx.core.*
import io.vertx.core.http.HttpHeaders
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.core.tracing.TracingPolicy
import io.vertx.ext.healthchecks.HealthCheckHandler
import io.vertx.ext.healthchecks.Status
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.CorsHandler
import io.vertx.ext.web.handler.HttpException
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.openapi.router.RequestExtractor
import io.vertx.ext.web.openapi.router.RouterBuilder
import io.vertx.openapi.contract.MediaType
import io.vertx.openapi.contract.OpenAPIContract
import io.vertx.openapi.validation.SchemaValidationException
import io.vertx.openapi.validation.ValidatedRequest
import java.util.*

/**
 * Create a pipe connector
 *
 * @param vertx The vertx instance
 * @param deploymentOptions The deployment options for the internally used verticle
 * @return Future of the connector
 */
@JvmOverloads
fun createPipeConnector(
    vertx: Vertx = Vertx.vertx(),
    deploymentOptions: DeploymentOptions = DeploymentOptions()
) = PipeConnector.create(vertx, deploymentOptions)

/**
 * Create a pipe connector
 *
 * @param vertx The vertx instance
 * @param deploymentOptions The deployment options for the internally used verticle
 * @param handler Handler function which is called when the creation finished
 * @return Future of the connector
 */
@JvmOverloads
fun createPipeConnector(
    vertx: Vertx = Vertx.vertx(),
    deploymentOptions: DeploymentOptions = DeploymentOptions(),
    handler: (AsyncResult<PipeConnector>) -> Unit
) = createPipeConnector(vertx, deploymentOptions).onComplete(handler)

/**
 * Create a pipe connector
 *
 * @param vertx The vertx instance
 * @param deploymentOptions The deployment options for the internally used verticle
 * @param handler Handler object which is called when the creation finished
 * @return Future of the connector
 */
@JvmOverloads
fun createPipeConnector(
    vertx: Vertx = Vertx.vertx(),
    deploymentOptions: DeploymentOptions = DeploymentOptions(),
    handler: Handler<AsyncResult<PipeConnector>>
) = createPipeConnector(vertx, deploymentOptions).onComplete(handler)

/**
 * The pipe connector
 *
 * The connector opens an http endpoint for receiving pipes. When a pipe is received, the connector either calls a
 * previously installed handler or publish it to a specific address of the Vert.x event bus.
 */
class PipeConnector : AbstractVerticle() {

    private var pipeHandler: PipeHandler? = null

    private var pipeMailer: PipeMailer? = null

    private lateinit var router: Router
    private lateinit var client: WebClient

    private var bodyThreshold: Int = 3145728

    private var buildInfo = JsonObject()
        .put("timestamp", "build time not available")
        .put("version", "version not available")

    override fun start(startPromise: Promise<Void>) {

//        val metrics = MetricsService.create(vertx)

        client = WebClient.create(vertx)

        vertx.fileSystem().readFileBlocking("buildInfo.json")?.let {
            buildInfo = it.toJsonObject()
        }

        // Force cache init during startup because it takes some time.
        Run.isCached("dummy")

        val storeOptions = ConfigStoreOptions()
            .setType("env")
            .setConfig(
                JsonObject().put(
                    "keys", JsonArray()
                        .add("PIVEAU_PIPE_ENDPOINT_PORT")
                        .add("PIVEAU_PIPE_MAIL_CONFIG")
                        .add("PIVEAU_PIPE_BODY_THRESHOLD")
                        .add("PIVEAU_PIPE_HTTP_BODY_LIMIT")
                        .add("PIVEAU_LOGO_PATH")
                        .add("PIVEAU_FAVICON_PATH")
                        .add("smtp_host")
                        .add("smtp_user")
                        .add("smtp_pwd")
                        .add("smtp_domain")
                )
            )

        var faviconPath = "webroot/images/favicon.png"
        var logoPath = "webroot/images/logo.png"

        val retriever = ConfigRetriever.create(vertx, ConfigRetrieverOptions().addStore(storeOptions))
        retriever.config
            .compose { config ->
                if (config.containsKey("smtp_host")) {
                    val mailConfig = JsonObject()
                        .mergeIn(config.getJsonObject("PIVEAU_PIPE_MAIL_CONFIG", JsonObject()))
                        .put("host", config.getString("smtp_host").trim())
                        .put("username", config.getString("smtp_user"))
                        .put("password", config.getString("smtp_pwd"))
                        .put("domain", config.getString("smtp_domain").trim())
                    pipeMailer = create(vertx, mailConfig)
                } else if (config.containsKey("PIVEAU_PIPE_MAIL_CONFIG")) {
                    val mailConfig = config.getJsonObject("PIVEAU_PIPE_MAIL_CONFIG")
                    pipeMailer = create(vertx, mailConfig)
                }

                bodyThreshold = config.getInteger("PIVEAU_PIPE_BODY_THRESHOLD", bodyThreshold)

                val bodyLimit = config.getLong("PIVEAU_PIPE_HTTP_BODY_LIMIT", 20971520)

                faviconPath = config.getString("PIVEAU_FAVICON_PATH", faviconPath)
                logoPath = config.getString("PIVEAU_LOGO_PATH", logoPath)

                OpenAPIContract.from(vertx, "webroot/openapi-pipe.yaml")
                    .compose { contract ->
                        val builder = RouterBuilder.create(vertx, contract, RequestExtractor.withBodyHandler())

                        builder.rootHandler(CorsHandler.create().allowedMethods(setOf(HttpMethod.POST)))

                        builder.rootHandler(BodyHandler.create().setBodyLimit(bodyLimit))

                        builder.getRoute("incomingPipe").addHandler(::incomingPipeHandler)
                        builder.getRoute("listRuns").addHandler(::listRunsHandler)
                        builder.getRoute("getRun").addHandler(::getRunHandler)
                        builder.getRoute("deleteRun").addHandler(::deleteRunHandler)
                        builder.getRoute("configSchema").addHandler(::configSchemaHandler)

                        router = builder.createRouter()
                        router["/pipe/schema"].order(0).handler(::pipeSchemaHandler)

                        router.route("/images/favicon").handler { serveResource(faviconPath, it) }
                        router.route("/images/logo").handler { serveResource(logoPath, it) }

                        val hch = HealthCheckHandler.create(vertx)
                        hch.register("buildInfo") { status -> status.complete(Status.OK(buildInfo)); }
                        router["/health"].handler(hch)

                        router.route("/*").handler(StaticHandler.create().setIndexPage("index-pipe.html"))

                        router.errorHandler(500) { context ->
                            val cause = context.failure()
                            if (cause is SchemaValidationException) {
                                val error = JsonObject()
                                    .put("status", "error")
                                    .put("message", cause.message)
                                context.response().setStatusCode(400)
                                    .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                    .end(error.toBuffer())
                            }
                        }

                        router.errorHandler(400) { context ->
                            val message = when (val failure: Throwable = context.failure()) {
                                is SchemaValidationException -> failure.message
                                is HttpException -> failure.payload
                                else -> failure.cause?.message ?: failure.javaClass.name
                            }
                            val error = JsonObject()
                                .put("status", "error")
                                .put("message", message)
                            context.response().setStatusCode(400)
                                .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                .end(error.encodePrettily())
                        }

                        val serverOptions = HttpServerOptions().setPort(
                            config.getInteger("PIVEAU_PIPE_ENDPOINT_PORT", 8080)
                        ).setTracingPolicy(TracingPolicy.PROPAGATE)

                        vertx.createHttpServer(serverOptions).requestHandler(router).listen()
                    }.mapEmpty<Void>().onComplete(startPromise)
            }
    }

    /**
     * Set handler for incoming pipe objects
     *
     * @param handler Handler function that will be called when the connector receives a pipe
     */
    fun handlePipe(handler: (PipeContext) -> Unit) {
        pipeHandler = SimplePipeHandler(vertx, handler)
    }

    /**
     * Set handler for incoming pipe objects
     *
     * @param handler Handler object that will be called when the connector receives a pipe
     */
    fun handlePipe(handler: Handler<PipeContext>) = handlePipe(handler::handle)

    @JvmOverloads
    fun publishTo(address: String, synchronous: Boolean = false) {
        pipeHandler = EventbusPipeHandler(address, vertx.eventBus(), synchronous)
    }

    fun subRouter(mountPoint: String, router: Router) = this.router.route(mountPoint).subRouter(router)

    private fun incomingPipeHandler(routingContext: RoutingContext) {
        val validatedRequest: ValidatedRequest = routingContext[RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST]
        val body = validatedRequest.body.jsonObject.encode()

        when {
            body.length > bodyThreshold -> vertx.executeBlocking { promise ->
                promise.complete(PipeManager.read(body))
            }
            else -> Future.succeededFuture(PipeManager.read(body))
        }.map { it.pipe }.onSuccess { pipe ->

            with(pipe.header) {
                if (runId == null) {
                    runId = "${pipe.header.name}~${UUID.randomUUID()}"
                }
                if (startTime == null) {
                    startTime = Date()
                }
            }

            val pipeContext = PipeContext(pipe, this)

            val run = Run.getOrCreate(pipe)

            when {
                run.isCanceled() -> routingContext.response()
                    .setStatusCode(HttpResponseStatus.CONFLICT.code())
                    .putHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                    .end(run.status.let(::filterStatus).encodePrettily())

                pipeHandler != null -> pipeHandler?.handle(pipeContext)
                    ?.onSuccess {
                        if (it.isEmpty) {
                            routingContext.response().setStatusCode(HttpResponseStatus.ACCEPTED.code())
                                .putHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                                .end(run.status.let(::filterStatus).encodePrettily())
                        } else {
                            val status = run.status.copy()
                            status.put("capsule", it)
                            routingContext.response()
                                .setStatusCode(HttpResponseStatus.OK.code())
                                .putHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                                .end(status.let(::filterStatus).encodePrettily())
                        }
                    }
                    ?.onFailure { routingContext.response().setStatusCode(500).end() }

                else -> routingContext.response().setStatusCode(501).end()
            }
        }.onFailure {
            routingContext.response().setStatusCode(HttpResponseStatus.BAD_REQUEST.code()).end(it.message)
        }
    }

    private fun listRunsHandler(routingContext: RoutingContext) {
        val list = Run.list()
        routingContext.response()
            .putHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .end(JsonArray(list.map { it.status.let(::filterStatus) }).encodePrettily())
    }

    private fun getRunHandler(routingContext: RoutingContext) {
        val validatedRequest: ValidatedRequest = routingContext[RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST]
        val runId = validatedRequest.pathParameters["runId"]!!.string
        when {
            Run.isCached(runId) -> routingContext.response()
                .setStatusCode(HttpResponseStatus.OK.code())
                .putHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .end(Run.get(runId)?.status?.let(::filterStatus)?.encodePrettily() ?: "{}")

            else -> routingContext.response().setStatusCode(HttpResponseStatus.NOT_FOUND.code())
                .setStatusMessage("Run Not Found").end()
        }
    }

    private fun deleteRunHandler(routingContext: RoutingContext) {
        val validatedRequest: ValidatedRequest = routingContext[RouterBuilder.KEY_META_DATA_VALIDATED_REQUEST]
        val runId = validatedRequest.pathParameters["runId"]!!.string
        when {
            Run.isCached(runId) -> {
                Run.get(runId)?.let { run ->
                    run.cancel()

                    // Propagate cancel
                    val pipeManager = PipeManager.manage(run.pipe)

                    val logger = PipeLogger(pipeManager.pipe.header, pipeManager.currentSegment?.header!!)

                    pipeManager.nextEndpoint?.address?.let { address ->
                        client.deleteAbs("$address/$runId").send()
                            .onSuccess { response ->
                                when (response.statusCode()) {
                                    HttpResponseStatus.NO_CONTENT.code() -> logger.info("Run $runId cancellation propagated to ${pipeManager.nextSegment?.header?.name}")
                                    else -> logger.warn("Cancellation propagation failed: ${response.statusMessage()}")
                                }
                            }
                            .onFailure { logger.error("Cancellation propagation failed") }
                    }
                }
                routingContext.response().setStatusCode(HttpResponseStatus.NO_CONTENT.code()).end()
            }

            else -> routingContext.fail(HttpResponseStatus.NOT_FOUND.code())
        }
    }

    private fun configSchemaHandler(routingContext: RoutingContext) {
        vertx.fileSystem().readFile("config.schema.json")
            .onSuccess {
                routingContext.response().putHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON).end(it)
            }
            .onFailure { routingContext.fail(HttpResponseStatus.NOT_FOUND.code()) }
    }

    private fun pipeSchemaHandler(routingContext: RoutingContext) {
        vertx.fileSystem().readFile("piveau-pipe.schema.json")
            .onSuccess {
                routingContext.response().putHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON).end(it)
            }
            .onFailure { routingContext.fail(HttpResponseStatus.NOT_FOUND.code()) }
    }

    fun forward(pipeContext: PipeContext) {
        // touch run cache
        pipeContext.isRunActive()

        if (!pipeContext.isForwarded()) {
            when (pipeContext.nextEndpoint?.protocol) {
                "eventbus" -> {
                    val address = pipeContext.nextEndpoint?.address
                    vertx.eventBus().send(address, pipeContext.getFinalBuffer())
                }

                "http", null -> vertx.eventBus().send(ForwardVerticle.ADDRESS, pipeContext.getFinalBuffer())
                else -> {}
            }
        }
    }

    fun pass(pipeContext: PipeContext) {
        if (!pipeContext.isForwarded()) {
            if (pipeContext.pipeManager.isBase64Payload) {
                pipeContext.setResult(pipeContext.binaryData, pipeContext.mimeType, pipeContext.dataInfo)
            } else {
                pipeContext.setResult(pipeContext.stringData, pipeContext.mimeType, pipeContext.dataInfo)
            }
            vertx.eventBus().send(ForwardVerticle.ADDRESS, pipeContext.getFinalBuffer())
        }
    }

    val isMailerEnabled: Boolean
        get() = pipeMailer != null

    fun useMailer(): PipeMailer? = pipeMailer

    private fun serveResource(resource: String, context: RoutingContext) {
        if (resource.startsWith("https://", ignoreCase = true) || resource.startsWith(
                "http://",
                ignoreCase = true
            )
        ) {
            client.getAbs(resource).send()
                .onSuccess { context.response().end(it.bodyAsBuffer()) }
                .onFailure { context.fail(HttpResponseStatus.NOT_FOUND.code()) }
        } else {
            context.response().sendFile(resource)
        }
    }

    private fun filterStatus(status: JsonObject): JsonObject {
        val filtered = status.copy()
        val pipe = filtered.remove("pipe") as JsonObject
        return filtered.put("pipeHeader", pipe.getJsonObject("header", JsonObject()))
    }

    companion object {
        @JvmStatic
        @JvmOverloads
        fun create(
            vertx: Vertx = Vertx.vertx(),
            deploymentOptions: DeploymentOptions = DeploymentOptions()
        ): Future<PipeConnector> {
            val pipeConnector = PipeConnector()
            vertx.eventBus().registerCodec(PipeContextMessageCodec(pipeConnector))

            val connectorFuture = vertx.deployVerticle(pipeConnector, deploymentOptions)
            val forwardFuture = vertx.deployVerticle(ForwardVerticle(), deploymentOptions)

            return Future.join(connectorFuture, forwardFuture).map { pipeConnector }
        }

        @JvmStatic
        @JvmOverloads
        fun create(
            vertx: Vertx = Vertx.vertx(),
            deploymentOptions: DeploymentOptions = DeploymentOptions(),
            handler: (AsyncResult<PipeConnector>) -> Unit
        ): Future<PipeConnector> = create(vertx, deploymentOptions).onComplete(handler)

        @JvmStatic
        @JvmOverloads
        fun create(
            vertx: Vertx = Vertx.vertx(),
            deploymentOptions: DeploymentOptions = DeploymentOptions(),
            handler: Handler<AsyncResult<PipeConnector>>
        ): Future<PipeConnector> = create(vertx, deploymentOptions).onComplete(handler)
    }

}