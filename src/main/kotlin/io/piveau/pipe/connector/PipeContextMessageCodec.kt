package io.piveau.pipe.connector

import io.piveau.pipe.PipeContext
import io.piveau.pipe.model.Pipe
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.MessageCodec

class PipeContextMessageCodec(private val pipeConnector: PipeConnector) : MessageCodec<Pipe, PipeContext?> {

    override fun encodeToWire(buffer: Buffer, entries: Pipe?) {}

    override fun decodeFromWire(i: Int, buffer: Buffer): PipeContext? = null

    override fun transform(entries: Pipe): PipeContext = PipeContext(entries, pipeConnector)

    override fun name(): String = javaClass.simpleName

    override fun systemCodecID(): Byte = -1

}