package io.piveau.pipe.connector

import io.piveau.pipe.PipeManager
import io.piveau.pipe.model.Pipe
import io.piveau.pipe.model.prettyPrint
import io.piveau.pipe.model.prettyPrintJson
import io.piveau.pipe.model.readPipeJson
import io.vertx.core.json.JsonObject
import org.ehcache.Cache
import org.ehcache.config.builders.CacheConfigurationBuilder
import org.ehcache.config.builders.CacheManagerBuilder
import org.ehcache.config.builders.ExpiryPolicyBuilder
import org.ehcache.config.builders.ResourcePoolsBuilder
import java.time.Duration
import java.time.Instant

class Run private constructor(val id: String, val status: JsonObject) {

    fun isActive(): Boolean = status.getString("status") == ACTIVE

    fun isFinished(): Boolean = status.getString("status") == FINISHED

    fun finish() {
        status
            .put("status", FINISHED)
            .put("endTime", Instant.now())
    }

    fun isCanceled(): Boolean = status.getString("status") == CANCELED

    fun cancel() {
        status
            .put("status", CANCELED)
            .put("endTime", Instant.now())
    }

    fun isFailed(): Boolean = status.getString("status") == FAILED

    @JvmOverloads
    fun failed(reason: String? = null) {
        status
            .put("status", FAILED)
            .put("endTime", Instant.now())
        if (reason != null) {
            status.put("reason", reason)
        }
    }

    val pipe: Pipe
        get() = readPipeJson(status.getJsonObject("pipe").encode())

    companion object {
        const val ACTIVE = "active"
        const val FINISHED = "finished"
        const val CANCELED = "canceled"
        const val FAILED = "failed"

        private val cacheManager = CacheManagerBuilder
            .newCacheManagerBuilder()
            .withCache(
                "runCache", CacheConfigurationBuilder
                    .newCacheConfigurationBuilder(
                        String::class.java,
                        JsonObject::class.java,
                        ResourcePoolsBuilder.heap(100)
                    )
                    .withExpiry(
                        ExpiryPolicyBuilder
                            .timeToIdleExpiration(Duration.ofHours(24))
                    )
            )
            .build(true)

        private val cache: Cache<String, JsonObject> =
            cacheManager.getCache("runCache", String::class.java, JsonObject::class.java)

        fun list(): List<Run> = cache.map { Run(it.key, it.value) }.toList()

        fun get(id: String): Run? = when {
            cache.containsKey(id) -> Run(id, cache[id])
            else -> null
        }

        fun getOrCreate(pipe: Pipe): Run {
            val id = pipe.header.runId!!
            return when {
                cache.containsKey(id) -> Run(id, cache[id])
                else -> {
                    val status = JsonObject()
                        .put("status", ACTIVE)
                        .put("pipe", JsonObject(pipe.prettyPrintJson()))
                    cache.put(id, status)
                    Run(id, status)
                }
            }
        }

        fun isCached(id: String) = cache.containsKey(id)
    }
}

