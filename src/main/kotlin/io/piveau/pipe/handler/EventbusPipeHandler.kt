package io.piveau.pipe.handler

import io.piveau.pipe.PipeContext
import io.piveau.pipe.connector.PipeContextMessageCodec
import io.vertx.core.Future
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.eventbus.EventBus
import io.vertx.core.json.JsonObject
import io.vertx.core.tracing.TracingPolicy

class EventbusPipeHandler(val address: String, private val eventBus: EventBus, synchronous: Boolean = false) :
    PipeHandler {

    private val handler = when {
        synchronous -> ::synchronous
        else -> ::asynchronous
    }

    override fun handle(pipeContext: PipeContext): Future<JsonObject> = handler.invoke(pipeContext)

    private fun synchronous(pipeContext: PipeContext) = eventBus.request<JsonObject>(
        address,
        pipeContext.pipe,
        DeliveryOptions()
            .setTracingPolicy(TracingPolicy.PROPAGATE)
            .setCodecName(PipeContextMessageCodec::class.simpleName)
    ).map { it.body() }

    private fun asynchronous(pipeContext: PipeContext): Future<JsonObject> {
        eventBus
            .send(
                address,
                pipeContext.pipe,
                DeliveryOptions()
                    .setTracingPolicy(TracingPolicy.PROPAGATE)
                    .setCodecName(PipeContextMessageCodec::class.simpleName)
            )
        return Future.succeededFuture(JsonObject())
    }

}