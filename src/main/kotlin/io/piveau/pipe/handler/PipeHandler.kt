package io.piveau.pipe.handler

import io.piveau.pipe.PipeContext
import io.vertx.core.Future
import io.vertx.core.json.JsonObject

interface PipeHandler {
    fun handle(pipeContext: PipeContext): Future<JsonObject>
}