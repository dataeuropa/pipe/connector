package io.piveau.pipe.handler

import io.piveau.pipe.ForwardVerticle
import io.piveau.pipe.PipeContext
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject

class SimplePipeHandler(private val vertx: Vertx, private val handler: (PipeContext) -> Unit) : PipeHandler {
    override fun handle(pipeContext: PipeContext): Future<JsonObject> = vertx.executeBlocking<Unit> { blocked ->
        handler.invoke(pipeContext)
        if (pipeContext.isFailure()) {
            blocked.fail(pipeContext.cause)
        } else {
            if (pipeContext.shouldForward()) {
                vertx.eventBus().send(ForwardVerticle.ADDRESS, pipeContext.getFinalBuffer())
            }
            blocked.complete()
        }
    }.onSuccess {
        pipeContext.log().trace("Incoming pipe successfully processed by handler.")
    }.onFailure {
        pipeContext.log().error("Handling pipe", it)
    }.map { JsonObject() }
}