package io.piveau.pipe.connector

import io.piveau.pipe.PipeContext
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.Message
import io.vertx.core.http.HttpResponseExpectation
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.codec.BodyCodec
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.properties.Delegates

@DisplayName("Testing the pipe connector")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PipeConnectorTest {

    private var connector: PipeConnector by Delegates.notNull()

    private var client: WebClient by Delegates.notNull()

    @BeforeAll
    @DisplayName("Initialize connector")
    fun createConnector(vertx: Vertx, testContext: VertxTestContext) {
        PipeConnector.create(vertx)
            .onSuccess {
                connector = it
                client = WebClient.create(vertx)
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }

    @Test
    @DisplayName("Testing handler")
    fun testHandler(vertx: Vertx, testContext: VertxTestContext) {
        connector.handlePipe {
            println(it.pipe.header.title)
            testContext.completeNow()
        }
        webInjection(vertx, "test-pipe.json")
            .onSuccess { testContext.completeNow() }
            .onFailure(testContext::failNow)
    }

    @Test
    @DisplayName("Testing producer")
    fun testProducer(vertx: Vertx, testContext: VertxTestContext) {
        vertx.eventBus().consumer(
            "io.piveau.pipe.connector.testing"
        ) { msg: Message<PipeContext> ->
            val pipeContext = msg.body()
            println(pipeContext.pipe.header.title)
            testContext.completeNow()
        }
        connector.publishTo("io.piveau.pipe.connector.testing")
        webInjection(vertx, "test-pipe.json")
            .onSuccess { testContext.completeNow() }
            .onFailure(testContext::failNow)
    }

    private fun webInjection(
        vertx: Vertx,
        fileName: String
    ): Future<HttpResponse<Buffer>> {
        return vertx.fileSystem().readFile(fileName)
            .map { JsonObject(it) }
            .compose {
                client.post(8080, "localhost", "/pipe")
                    .sendJsonObject(it)
                    .expecting(HttpResponseExpectation.SC_ACCEPTED)
            }
    }

}