package io.piveau.pipe.connector

import io.piveau.pipe.PipeContext
import io.vertx.core.Vertx
import io.vertx.core.eventbus.Message
import io.vertx.core.http.HttpResponseExpectation
import io.vertx.core.json.JsonObject
import io.vertx.core.json.pointer.JsonPointer
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.codec.BodyCodec
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.properties.Delegates

@DisplayName("Testing the run API")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class RunTest {
    private var connector: PipeConnector by Delegates.notNull()

    private var client: WebClient by Delegates.notNull()

    private val runIdPointer = JsonPointer.from("/pipeHeader/runId")

    private var runId: String? = null

    @BeforeAll
    @DisplayName("Initialize connector")
    fun createConnector(vertx: Vertx, testContext: VertxTestContext) {
        PipeConnector.create(vertx)
            .onSuccess {
                connector = it
                client = WebClient.create(vertx)

                vertx.eventBus()
                    .consumer("io.piveau.pipe.connector.testing") { _: Message<PipeContext> -> }

                connector.publishTo("io.piveau.pipe.connector.testing")

                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }

    @Test
    @DisplayName("Testing start run")
    @Order(1)
    fun testStartRun(vertx: Vertx, testContext: VertxTestContext) {
        vertx.fileSystem().readFile("test-pipe.json")
            .map { JsonObject(it) }
            .compose { pipe ->
                client.post(8080, "localhost", "/pipe")
                    .`as`(BodyCodec.jsonObject())
                    .sendJsonObject(pipe)
                    .expecting(HttpResponseExpectation.SC_ACCEPTED.and(HttpResponseExpectation.JSON))
                    .map { it.body() }
            }
            .onSuccess { status ->
                println(status.encodePrettily())
                runId = runIdPointer.queryJson(status).toString()
                testContext.verify {
                    Assertions.assertEquals("active", status.getString("status"))
                }
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }

    @Test
    @DisplayName("Testing run status")
    @Order(2)
    fun testGetRunStatus(vertx: Vertx, testContext: VertxTestContext) {
        client.get(8080, "localhost", "/pipe/$runId")
            .`as`(BodyCodec.jsonObject())
            .send()
            .expecting(HttpResponseExpectation.SC_OK.and(HttpResponseExpectation.JSON))
            .map { it.body() }
            .onSuccess {
                println(it.encodePrettily())
                testContext.verify {
                    Assertions.assertEquals("active", it.getString("status"))
                }
                testContext.completeNow() }
            .onFailure(testContext::failNow)
    }

    @Test
    @DisplayName("Testing cancel run")
    @Order(3)
    fun testCancelRun(vertx: Vertx, testContext: VertxTestContext) {
        client.delete(8080, "localhost", "/pipe/$runId")
            .send()
            .expecting(HttpResponseExpectation.SC_NO_CONTENT)
            .compose {
                client.get(8080, "localhost", "/pipe/$runId")
                    .`as`(BodyCodec.jsonObject())
                    .send()
                    .expecting(HttpResponseExpectation.SC_OK.and(HttpResponseExpectation.JSON))
            }
            .onSuccess { testContext.completeNow() }
            .onFailure(testContext::failNow)
    }

    @Test
    @DisplayName("Testing list runs")
    @Order(4)
    fun testListRuns(vertx: Vertx, testContext: VertxTestContext) {
        client.get(8080, "localhost", "/pipe/runs")
            .`as`(BodyCodec.jsonArray())
            .send()
            .expecting(HttpResponseExpectation.SC_OK.and(HttpResponseExpectation.JSON))
            .map { it.body() }
            .onSuccess { runs ->
                testContext.verify {
                    Assertions.assertEquals("canceled", (runs.find { runIdPointer.queryJson(it as JsonObject) == runId } as JsonObject).getString("status"))
                }
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }

}